
function executaFormulario(){

  if(validacao()){
    return;
  }
  
  let nome = document.getElementById('nome').value;
  let sobrenome = document.getElementById('sobrenome').value;
  let estado = document.getElementById('estado').value;
  let cidade = document.getElementById('cidade').value;
  let exibeDados = document.getElementById('exibeDados');

  //criando um objeto pessoa
  let pessoa = {
    'Nome': nome,
    'Sobrenome': sobrenome,
    'Estado': estado,
    'Cidade': cidade
  }
  //transformando o objeto pessoa em uma array
  let chaves = Object.keys(pessoa);
  
  //percorrendo o array de pessoa
  for (let item of chaves) {
    exibeDados.innerHTML += `<p>${item}: ${pessoa[item]}</p>`;
  }
}


function exibirCidade(){
  let listaCidades = [
    {
      "estado": "RS",
      "cidades": [
        {
          "nome": "Santa Cruz do Sul",
          "id": "santacruz"
        },
        {
          "nome": "Porto Alegre",
          "id": "portoalegre"
        }
      ]
    },
    {
      "estado": "SC",
      "cidades": [
        {
          "nome": "Florianópoles",
          "id": "floripa"
        },
        {
          "nome": "Blumenau",
          "id": "blumenau"
        }
      ]
    }
  ];
  
  let estado = document.getElementById('estado').value;
  let cidade = document.getElementById('cidade');
  let existeEstado = false;

  for (let item of listaCidades) {
    if(item.estado == estado){
      existeEstado = true;
      let option = "";
      for (let item2 of item.cidades) {
        option += `<option value="${item2.id}">${item2.nome}</option>`;
      }
      cidade.style.display = "initial";
      cidade.innerHTML = option;
    }
  }

  if(!existeEstado){
    cidade.style.display = "none";
  }
}

function validacao(){
  let temErro = false;

  let nome = document.getElementById('nome').value;
  let nomeErro = criarElementoErro('nome');
  if(nome == ""){
    nomeErro.innerHTML = "Preencha um nome válido!";
    nomeErro.style.display = "initial";
    temErro = true;
  }else{
    nomeErro.style.display = "none";
  }

  let sobrenome = document.getElementById('sobrenome').value;
  let sobrenomeErro = criarElementoErro('sobrenome');
  if(sobrenome == ""){
    sobrenomeErro.innerHTML = "Preencha um sobrenome válido!";
    sobrenomeErro.style.display = "initial";
    temErro = true;
  }else{
    sobrenomeErro.style.display = "none";
  }

  let estado = document.getElementById('estado').value;
  let estadoErro = criarElementoErro('estado');
  if(estado == ""){
    estadoErro.innerHTML = "Preencha um estado válido!";
    estadoErro.style.display = "initial";
    temErro = true;
  }else{
    estadoErro.style.display = "none";
  }

  return temErro;
}

function criarElementoErro(idAlvo){
  //pego elemento que eu quero trabalhar em cima
  let elementoAlvo = document.getElementById(idAlvo);
  //lógica para a exibição do span ficar dinamica no HMTL
  let nodePai = elementoAlvo.parentNode; //retorna a estrutura Pai
  //valido se aquele elemento de erro ja nao está sendo exibido
  if(!document.getElementById(idAlvo+'Erro')){
    let valorAlvo = document.getElementById(idAlvo).value; //recupera o valor do campo
    //concateno o elemento que estou trabalhando mais o span de erro
    nodePai.innerHTML += `<span id="${idAlvo}Erro" style="display: none;color: red;"></span>`;
    //recupero o valor do campo, pois o innterHTML limpa esse valor
    document.getElementById(idAlvo).value = valorAlvo;
  }
  //retorno o elemento span (com o erro) para o metodo que chamar esse
  return document.getElementById(idAlvo+'Erro');
}