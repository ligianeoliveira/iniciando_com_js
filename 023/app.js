//função pra ser chamada direto no html
function pegaNome(){
  let nome = document.getElementById('nome').value;
  document.getElementById('nomeInput').innerHTML = nome;
}

//colocando a ação direto no botao, sem precisa chama no html
//Forma 2
var btnTeste1 = document.getElementById('btnTeste1');
btnTeste1.addEventListener('click', function(){
  let nome = document.getElementById('nome').value;
  document.getElementById('nomeInput').innerHTML = nome;
});

//colocando tb evento no input - Forma 2
document.getElementById('nome').addEventListener('keyup', function(){
  let nome = document.getElementById('nome').value;
  document.getElementById('nomeInput').innerHTML = nome;
});

// colocando a ação direto no input, sem precisa chama no html
/* Forma 1
var inputNome = document.getElementById('nome');
inputNome.onkeyup = function(){
  let nome = document.getElementById('nome').value;
  document.getElementById('nomeInput').innerHTML = nome;
}
*/

/* colocando a ação direto no botao, sem precisa chama no html
Forma 1
var btnTeste1 = document.getElementById('btnTeste1');
btnTeste1.onclick = function(){
  let nome = document.getElementById('nome').value;
  document.getElementById('nomeInput').innerHTML = nome;
}
*/