
class Util{
  static lista(lista){
    let html = '<ul>';
    for (var item of lista) {
      html += `
      <li>${item}</li>
      `;
    }
    html += '</ul>';
    return html;
  }
}

let lista = ["Ligiane", "Camila", "Gustavo"];
document.getElementById('listaUsuarios').innerHTML = Util.lista(lista);
