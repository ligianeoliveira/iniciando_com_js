
function executaFormulario(){

  if(validacao()){
    return;
  }
  
  let nome = document.getElementById('nome').value;
  let sobrenome = document.getElementById('sobrenome').value;
  let estado = document.getElementById('estado').value;
  let cidade = document.getElementById('cidade').value;

  let exibeDados = document.getElementById('exibeDados');
  exibeDados.innerHTML += `<p>Nome: ${nome}</p>`;
  exibeDados.innerHTML += `<p>Sobrenome: ${sobrenome}</p>`;
  exibeDados.innerHTML += `<p>Estado: ${estado}</p>`;
  exibeDados.innerHTML += `<p>Cidade: ${cidade}</p>`;
}


function exibirCidade(){
  let listaCidades = [
    {
      "estado": "RS",
      "cidades": [
        {
          "nome": "Santa Cruz do Sul",
          "id": "santacruz"
        },
        {
          "nome": "Porto Alegre",
          "id": "portoalegre"
        }
      ]
    },
    {
      "estado": "SC",
      "cidades": [
        {
          "nome": "Florianópoles",
          "id": "floripa"
        },
        {
          "nome": "Blumenau",
          "id": "blumenau"
        }
      ]
    }
  ];
  
  let estado = document.getElementById('estado').value;
  let cidade = document.getElementById('cidade');
  let existeEstado = false;

  for (let item of listaCidades) {
    if(item.estado == estado){
      existeEstado = true;
      let option = "";
      for (let item2 of item.cidades) {
        option += `<option value="${item2.id}">${item2.nome}</option>`;
      }
      cidade.style.display = "initial";
      cidade.innerHTML = option;
    }
  }

  if(!existeEstado){
    cidade.style.display = "none";
  }
}

function validacao(){
  let nome = document.getElementById('nome').value;
  let nomeErro = document.getElementById('nomeErro');
  let temErro = false;
  if(nome == ""){
    nomeErro.innerHTML = "Preencha um nome válido!";
    nomeErro.style.display = "initial";
    temErro = true;
  }else{
    nomeErro.style.display = "none";
  }
  return temErro;
}