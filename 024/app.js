//função pra ser chamada direto no html
function pegaNome(){
  let nome = document.getElementById('nome').value;
  document.getElementById('nomeInput').innerHTML = nome;
}

var btnTeste1 = document.getElementById('btnTeste1');
btnTeste1.addEventListener('click', function(){
  let nome = document.getElementById('nome').value;
  document.getElementById('nomeInput').innerHTML = nome;

  //lógica para travar o botão de vários clicks simultâneos
  this.style.display = "none";
  document.getElementById('btnTeste2').style.display = "initial";

  setTimeout(() => {
    document.getElementById('btnTeste1').style.display = "initial";
    document.getElementById('btnTeste2').style.display = "none";
  }, 1000);

});

